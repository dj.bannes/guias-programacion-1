import random
from time import sleep
tiempo = 0.5
import os
clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')



def imprimir(alea, matriz):#funcion que imprime la matriz 
    for i in range(alea+1):
        for j in range (alea):
            if i == 0:
                if j == 0:
                    print("   ", j+1,end="    ") 
                if j != 0 and j <=8:
                    print(j+1,end="    ")
                if j > 8:
                    print(j+1,end="   ")
           
            elif j == 0 and i != alea +1:
                if i<10:
                    print(i, "", matriz[i-1][j],end="  ")
                else:
                    print(i, matriz[i-1][j],end="  ")
            else:
                print(matriz[i-1][j],end="  ")

        print ( )

def Crearparticula(matriz):#creacion de particula random

    x = random.randrange(len(matriz))
    y = random.randrange(len(matriz))
    matriz[x][y] = "|*|"
    return [x,y]

def letras(matriz):#creacion de letras random

    x = random.randrange(len(matriz))
    y = random.randrange(len(matriz))

    if matriz[x][y] == "| |":
        matriz[x][y] = "|F|"

    x = random.randrange(len(matriz))
    y = random.randrange(len(matriz))
    if matriz[x][y] == "| |":
        matriz[x][y] = "|S|"

    x = random.randrange(len(matriz))
    y = random.randrange(len(matriz))
    if matriz[x][y] == "| |":
        matriz[x][y] = "|A|"

    x = random.randrange(len(matriz))
    y = random.randrange(len(matriz))
    if matriz[x][y] == "| |":
        matriz[x][y] = "|E|"

def movimiento(posp,matriz,N):#movimiento de particula
    while True:
        matriz[posp[0]][posp[1]] = "| |"#borra particula del mov anterior ### posp 0 = X posp 1 = Y
        w = random.randint(0,3)#4 movimientos posibles
        if w == 0: # se mueve a la izquierda
            if posp[1] != 0:#estrictamente distinto de 0
                posp[1] -= 1
                if matriz[posp[0]][posp[1]] == "|F|":
                    break
                elif matriz[posp[0]][posp[1]] == "|E|":
                    z = random.randint(0,1)
                    if z == 0:
                        posp[0]= 0
                        posp[1]= 0
                    else:
                        posp[0]= N-1
                        posp[1]= N-1
                elif matriz[posp[0]][posp[1]] == "|A|":
                    c = random.randint(0,2)
                    if c == 0: 
                        posp[1] += 2#retrocede                     
                    elif c == 1:
                        posp[1] += 1
                        posp[0] -= 1 
                    else:
                        posp[0] += 1
                        posp[1] += 1
        elif w == 1: # se mueve a la derecha
            if posp[1] != N-1:#estrictamente distinto de N-1
                posp[1] += 1
                if matriz[posp[0]][posp[1]] == "|F|":
                    break
                elif matriz[posp[0]][posp[1]] == "|E|":
                    z = random.randint(0,1)
                    if z == 0:
                        posp[0]= 0
                        posp[1]= 0
                    else:
                        posp[0]= N-1
                        posp[1]= N-1
                elif matriz[posp[0]][posp[1]] == "|A|":
                    c = random.randint(0,2)
                    if c == 0: 
                        posp[1] -= 2 #retrocede
                    
                    elif c == 1:
                        posp[1] -= 1
                        posp[0] -= 1 
                    else:
                        posp[0] += 1
                        posp[1] -= 1           
        elif w == 2: # se mueve hacia arriba
            if posp[0] != 0: #estrictamente distinto de 0
                posp[0] -= 1
                if matriz[posp[0]][posp[1]] == "|F|":
                    break
                elif matriz[posp[0]][posp[1]] == "|E|":
                    z = random.randint(0,1)
                    if z == 0:
                        posp[0]= 0
                        posp[1]= 0
                    else:
                        posp[0]= N-1
                        posp[1]= N-1
                elif matriz[posp[0]][posp[1]] == "|A|":
                    c = random.randint(0,2)
                    if c == 0: 
                        posp[0] += 2 #retrocede
                     
                    elif c == 1:
                        posp[1] += 1
                        posp[0] -= 1 
                    else:
                        posp[0] -= 1
                        posp[1] -= 1 
        else: # se mueve hacia abajo
            if posp[0]!= N-1:#estrictamente distinto de N-1
                posp[0] += 1
                if matriz[posp[0]][posp[1]] == "|F|":
                    break
                elif matriz[posp[0]][posp[1]] == "|E|":
                    z = random.randint(0,1)
                    if z == 0:
                        posp[0]= 0
                        posp[1]= 0
                    else:
                        posp[0]= N-1
                        posp[1]= N-1
                elif matriz[posp[0]][posp[1]] == "|A|":
                    c = random.randint(0,2)
                    if z == 0: 
                        posp[0] -= 2 #retrocede
                    
                    elif c == 1:
                        posp[1] += 1
                        posp[0] -= 1 
                    else:
                        posp[0] -= 1
                        posp[1] -= 1 

        sleep(tiempo)
        clearConsole()
        matriz[posp[0]][posp[1]] = "|*|"
        imprimir(len(matriz),matriz)
      
      

        while posp[0] in [0,N-1] or posp[1] in [0,N-1]:


            #rebotes          
            matriz[posp[0]][posp[1]] = "| |" # borra moviento anterior
            z = random.randint(0,2)
            if posp[0] == 0 and posp[1] == 0:
                if z == 0: # hacia abajo esquina superior izquierda
                    posp[0] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[1] += 1 # hacia la derecha  desde la esquina superior izquierda 
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1               
                else:
                    posp[0] += 1#diagonal esquina superior izquierda
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
            elif posp[0] == 0 and posp[1] == N-1:#esquina superior derecha 
                if z == 0:
                    posp[0] += 1 #hacia abajo esquina superior derecha
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[1] -= 1 #hacia atras esquina superior derecha
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] += 1 #diagonal desde esquina superior derecha
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
            elif posp[0] == N-1 and posp[1] == 0:#esquina inferior izquierda
                if z == 0:
                    posp[0] -= 1 #rebote hacia arriba 
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[1] += 1 #hacia la derecha  desde esquina inferior izquierda
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] -= 1#diagonal 
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
            elif posp[0] == N-1 and posp[1] == N-1: #esquina inferior derecha
                if z == 0:
                    posp[0] -= 1 #hacia arriba
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[1] -= 1# hacia la izquierda desde la esquina inferior derecha
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] -= 1#diagonal   desde la esquina inferior derecha 
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
            elif posp[0] == 0 and posp[1] in range(1,N-1):#pared superior
                if z == 0:
                    posp[0] += 1 #diagonal izquierda  pared superior 
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[0] += 1  #hacia abajo 
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] += 1#diagonal derecha hacia abajo 
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
            elif posp[0] == N-1 and posp[1] in range(1,N-1):#pared inferior
                if z == 0:
                    posp[0] -= 1 #hacia arriba rebote
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[0] -= 1# diagonal izquierda hacua arriba
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] -= 1# diagonal derecha hacia arriba
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1

            elif posp[0] in range(1,N-1) and posp[1] == 0:#pared izquierda 
                if z == 0:
                    posp[1] += 1 # hacia la izquierda  rebote
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[0] += 1 #diagonal hacia abajo 
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] -= 1 #diagonal hacia arriba
                    posp[1] += 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1

            elif posp[0] in range(1,N-1) and posp[1] == N-1:#pared derecha
                if z == 0:
                    posp[1] -= 1 #rebote hacia atras rectamente 
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                elif z == 1:
                    posp[0] -= 1 #diagonal derecha hacia arriba
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
                else:
                    posp[0] += 1 #hacia abajo diagonal 
                    posp[1] -= 1
                    if matriz[posp[0]][posp[1]] == "|F|":
                        break
                    elif matriz[posp[0]][posp[1]] == "|E|":
                        z = random.randint(0,1)
                        if z == 0:
                            posp[0]= 0
                            posp[1]= 0
                        else:
                            posp[0]= N-1
                            posp[1]= N-1
               


            
            matriz[posp[0]][posp[1]] = "|*|"  # imprime el robote de la particula
            sleep(tiempo)
            clearConsole()
            imprimir(len(matriz),matriz)


       
   
if __name__ == "__main__":

    N = random.randint(3 ,15)# matriz de tamaño N * N aleatoria 
    Matriz = []

    for i in range(N):
        Matriz.append([])
        for j in range (N):
            Matriz[i].append("| |")
    letras(Matriz)
    particula = Crearparticula(Matriz)
    imprimir(N,Matriz)
    movimiento(particula,Matriz,N)
   
  
   

   
#print (Matriz)
