'''
Una empresa constructora vende terrenos con la forma A de la figura. Se requiere crear un programa
para obtener el  ́area respectiva de un terreno de medidas de cualquier valor.
Figura 3: Representaci ́on gr ́afica de los puntos en el plano cartesiano.
Para resolver este problema se debe identificar que la forma A est ́a compuesta por dos figuras: un
tri ́angulo de base B y de altura (A - C); y por otro lado, un rect ́angulo que tiene base B y altura C y
la suma de ambas  ́areas es el resultado final.
'''
print("el area total del trapecio")
print("necesitamos las medidas A, B, C")
print("A es la parte mas larga")
print("B es la base ")
print("C es el lado mas corto")
A = int(input("ingrese el Dato A "))
B = int(input("ingrese el Dato B "))
C = int(input("ingrese el Dato C "))

CatetoY = A - C#calculo del catetoY del triangulo
CatetoX = B# el catetox es equivalente a la Base del rectangulo
Area_triangulo = CatetoX * CatetoY / 2
Area_rectangulo = B * C# el area es la muntiplicaicon de la base por la altura
Area_trapecio = Area_triangulo + Area_rectangulo# al sumar las dos areas tenemos el total que estamos buscando 

print("el area total del trapesio es:",Area_trapecio)