import math# importe match para ocupar pi
'''
Se requiere obtener el  ́area de la figura en la forma A de la siguiente imagen. Para resolver este problema
se puede partir de que est ́a formada por tres figuras: dos tri ́angulos rect ́angulos, con H como hipotenusa
y R como uno de los catetos, que tambi ́en es el radio de la otra figura, una semicircunferencia que forma
la parte circular (ver forma B)

Por lo tanto, para poder resolver el problema, se tiene que calcular el cateto faltante, que es la altura
del tri ́angulo, con  ́esta se puede calcular el  ́area del tri ́angulo, y para obtener el  ́area total triangular
se multiplicar ́a por dos. Por otro lado, para calcular el  ́area de la parte circular, se calcula el  ́area de
la circunferencia y luego se divide entre dos, ya que representa solo la mitad del c ́ırculo.
'''
print("calculando el Area")
print("debe ingresar 2 datos")
print("necesitamos el dato r que equivale al radio del circulo")
print("necesitamos el dato H que equivale a la hipotenusa")
r = int(input("ingrese el radio del circulo"))
H = int(input("ingrese la Hipotenusa del triangulo rectangulo"))

cateto = ((H**2 - r**2)**(0.5))#busqueda de la altura 
area_triangulo = cateto * r#area total del triangulo a encontrar
print ("Altura",cateto)
print("Hipotenusa",H)
print("cateto",r)
print("area total del triangulo a encontrar",area_triangulo)
area_total = math.pi * (r**2)#area total del circulo
print("area del circulo",area_total)
mitad_area_circular = area_total / 2# area total divido en 2 para encontrar la mitad del area del circulo
print("mitad de area del circulo",mitad_area_circular)
print("la suma de las areas es:",mitad_area_circular + area_triangulo)