'''
Un estacionamiento requiere determinar el cobro que debe aplicar a las personas que lo utilizan. Con-
sidere que el cobro es con base en las horas que lo disponen y que las fracciones de hora se toman como
completas. Cree un programa que realice el cobro del estacionamiento.
'''
print("EL ESTACIONAMIENTO")
print("el precio por hora sera ajustado a que si usted paso 1 hora ya se le cobrara la siguiente hora")
print("la hora cuesta 500 pesos")
print("debe colocar los datos en minutos ejemplo 1 hora = 60")
hora = int(input("cuanto tiempo estara en el estacionamiento"))#aqui pido el dato de cuantos minutos estara  en el estacionamiento

valor_hora = 500
if hora < 60:# aqui tranformo los valores a numeros mas faciles de calcular
   fracc_hora = 1
   calculadora = fracc_hora * valor_hora
   print("total a pagar es: ",calculadora)
if hora > 60 and hora < 120:# rangos donde cubre la segunda hora 
    fracc_hora = 2
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora >120 and hora < 180:# rangos de la tercera hora
    fracc_hora = 3
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 180 and hora <240:#rangos de la cuarta hora 
    fracc_hora = 4
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 240 and hora < 300:#rangos de la quinta hora
    fracc_hora = 5
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora >300 and hora < 360:#rangos de la sexta hora
    fracc_hora = 6
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 360 and hora < 420:#rangos de la septima hora
    fracc_hora = 7
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 420 and hora < 480:#rangos de la octava hora
    fracc_hora = 8
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora >480 and hora < 540:#rangos de la novena hora 
    fracc_hora = 9
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 540 and hora < 600:#arangos de la 10 hora
    fracc_hora = 10#si esta dentro del rango  lo tomara como hora completa 
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 600 and hora < 660:#rangos de la 11 hora
    fracc_hora = 11
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 660 and hora < 720:#rangos de la 12 hora
    fracc_hora = 12
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 720 and hora < 780:#rangos de la 13 hora
    fracc_hora = 13
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 780 and hora < 840:#rangos de la 14 hora
    fracc_hora = 14
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 840 and hora < 900:#rangos de la 15 hora
    fracc_hora = 15
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 900 and hora < 960:#rangos de la 16 hora
    fracc_hora = 16
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)
if hora > 960 and hora < 1020:#rangos de la 17 hora
    fracc_hora = 17
    calculadora = fracc_hora * valor_hora
    print("total a pagar es: ",calculadora)

else:
    print("excede las horas permitidas, reinicie el programa")
    

