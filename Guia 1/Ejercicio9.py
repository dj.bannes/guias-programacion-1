'''
Se requiere obtener la distancia entre dos puntos en el plano cartesiano, tal y como se muestra en la
figura siguiente.
Figura 2: Representaci ́on gr ́afica de los puntos en el plano cartesiano.
Para resolver este problema es necesario conocer las coordenadas de cada punto (X, Y), y con esto
poder obtener el cateto de abscisas (X1 y X2) y el de ordenadas (Y1 e Y2), y mediante estos valores
obtener la distancia entre P1 y P2, utilizando el teorema de Pit ́agoras.
'''
print("La distancia entre 2 puntos")
print("necesitamos las coordenas X1,Y1 y X2,Y2")
X1=int(input("ingrese la coordenada X1"))#el usuario necesita entregar estos 4 datos para hacer los calculos
Y1=int(input("ingrese la coordenada Y1"))
X2=int(input("ingrese la coordenada X2"))
Y2=int(input("ingrese la coordenada Y2"))

catetoX = X2 - X1 # el calculo del catetox
catetoY = Y2 - Y1# el calculo del  catetoy
Hipotenusa =((catetoX**2 + catetoY**2)**(0.5))# se hace uso del teorema de pitagoras para calcular la hipotenusa

print("el catetoX es",catetoX)
print("el catetoY es",catetoY)
print("la distancia entre los puntos es:",Hipotenusa)