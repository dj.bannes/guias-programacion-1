'''
Escriba un programa que pida al usuario dos palabras, y se indique cu´al de ellas es la m´as larga y por
cu´antas letras lo es.
'''
def letras():#funcion que recibe las palabras del usuriario
    palabras2 = []# guarda las palabras del usuario
    for i  in range(0, 2):# for con rango 2 para las 2 palabras a ingresar por el usuario
        palabra = input("escriba una palabra")#palabras escritas por el usuario
        palabras2.append(palabra)#append guarda las palabras en  una lista 
        print(palabras2)
    return palabras2# retorno al main 

def contador(palabras2):# funcion que cuenta la cantidad de letras 
    can_palabra1 = len(palabras2[0])#len verifica cuantas palabras hay en esa posicion
    can_palabra2 = len(palabras2[1])
    if  can_palabra1 > can_palabra2:# if, elif y else para verificar la palabra mayor
        print("la palabra 1 es mas larga por :",can_palabra1 - palabras2, "letras")
    elif can_palabra1 == can_palabra2:
        print("son iguales")
    else:
        print("la palabra 2 es mas larga por :",can_palabra2 - can_palabra1, "letras")



if __name__ == "__main__":
    pasapalabra = letras()# retorno al main y variable creada para entregarsela a contador
    contador(pasapalabra)