'''
Dada un String que se forman de manera aleatoria, devuelva una nueva lista cuyo contenido sea igual
a la original pero de modo invertida. Ejemplo:
Ejemplo:
"Di buen d ́ıa a Pap ́a"
Devuelva
"Pap ́a a d ́ıa buen Di"
'''
# importo la libreria random para utilizar su aleatoriedad
import random


def f_random(lista):#def = funcion llamo a list
    listanew = []#lista nueva donde se guardaran los nuevos resultados del random
    for i in lista:#for revisa la lista desde el 0 al 6 dependiendo del tamaño de la misma
        rando = random.randint(0,6)#randint entre parentesis tiene el inicio y el final de la lista a revolver
        listanew.append(lista[rando])#.append copia lo que esta entre parentesis a la lista indicada
    return listanew # se retorna listanew para asignarla a una nueva lista en el main
    
            

if __name__ == "__main__":
#creo la lista con distintos datos.
    list = ['Hola', 'pepe', 'como', 'esta', 'el', 'dia', 'F']
    listaramdom = f_random(list)#lista retornada desde la funcion con valores reagrupados
    print("lista original")
    print(list)
    print("lista random desde la original")
    print(listaramdom)
    print("lista random al reves")
    listaramdom.reverse()
    print(listaramdom)
    