'''
Escriba un programa que indique si una palabra existe dentro de un texto. La palabra como la oraci´on
ser´an ingresadas por el usuario.
'''
def buscarpalabra():#funcion que busca la palabra 
    frase = input("escriba su frase preferida")# frase ingresada por el usuario
    palabra = input("escriba una palabra para verificar si existe en su frase")#variable para buscar en la frase
     
    print(frase)# imprime la frase
    buscar = frase.count(palabra)#busca la palabra y cuenta cuantas veces esta en la frase
    if buscar > 0 :
        print("la palabra",palabra,"si existe",buscar,"veces")
    else:
        print("la palabra no existe en el texto")

if __name__ == "__main__":
    buscarpalabra()