'''
. Extienda el programa anterior para logre encontrar palabras dentro de un texto, sin importar si est´an
escritas en diferente combinaci´on de may´usculas/min´usculas.
'''
def buscarpalabra():# esta funciones una extension del ejercicio 3
    frase = input("escriba su frase preferida")#variable que  ingresa solo la frase
    frase2 = frase.upper()#nueva variable que convierte todo a mayuscula
    while True:# gracias al while se piden palabras hasta que el usuario desida lo contrario
        palabra = input("escriba una palabra para verificar si existe en su frase")
        palabra2 = palabra.upper()#nueva variable que convierte todo a mayuscula
        buscar = frase2.count(palabra2)# busca la palabra y cuenta cuantas veces esta en la frase
        if buscar > 0 :
            print("la palabra",palabra,"si existe",buscar,"veces")
        else:
            print("la palabra no existe en el texto")

        condicion = input("ingrese 1 si quiere dejar de ingresar palabras o cualquiero otro numero para continuar")
        if condicion == '1':
            break
    print(frase)
   


if __name__ == "__main__":
    buscarpalabra()